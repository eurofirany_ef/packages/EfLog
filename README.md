# EfLog

Package for creating logs

## Install
```bash
composer require eurofirany/ef-log
```

```bash
php artisan EfLog:install
```

## Configuration
```php
<?php

return [
    'app_name' => env('APP_NAME'),
    'app_env' => env('APP_ENV'),
    'app_url' => env('APP_URL'),
    'logs_url' => env('APP_URL').'/ef-log/logs/',
    'logs_token' => env('EF_TOKEN'),
    'logs_extended_tables' => [
        //
    ],
    'logs_teams_channel_url' => env('EF_LOG_TEAMS_CHANNEL_URL'),
];
```

## Variables for .env
```bash
EF_LOG_TEAMS_CHANNEL_URL=
```

## Log types
```bash
INFO
SUCCESS
ERROR
WARNING
```

## Commands for groups
```bash
php artisan EfLog:addGroup 
# Optional parameters
# --name= --translation=

php artisan EfLog:removeGroup
# Optional parameter
# --name=
```

## Commands for operations
```bash
php artisan EfLog:addOperation
# Optional parameters
# --name= --translation=

php artisan EfLog:removeOperation
# Optional parameter
# --name=
```

## Methods
```php
// Type
->info('TITLE') 
->success('TITLE')
->error('TITLE')
->warning('TITLE')

->description('DESC') // Optional

->group('GROUP')

->operation('OPERATION')

->properties('TABLE', [
    'KEY1' => 'VALUE1',
    'KEY2' => 'VALUE2'
]) // Optional - Save properties in extended table

->parent($parentId) // Optional

->save() // Save log

->send() // Send notification

->show(); // Show log
```

## Example usage
```php
#1
EfLog::create()
    ->success('title')
    ->description('description')
    ->properties('Files', [
        'name' => 'filename'
    ])
    ->group('Files')
    ->operation('Download')
    ->save()
    ->send() 
    ->show(); 

#2 
EfLog::create()
    ->error('title')
    ->parent('123')
    ->save();

#3 
EfLog::create()
    ->info('title')
    ->send();

#4 
EfLog::create()
    ->warning('title')
    ->description('description')
    ->show();
```

## Extended tables
```bash
php artisan EfLog:extend
```

## Define table in config
```php
<?php
// ef_log

return [
    'logs_extended_tables' => [
        'Files', 
    ]
];
```

## Example model for extended table
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EfLogExtendedTableFilesLog extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'log_id',
        'name'
    ];
}


```

## Example migration for extended table
```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEfLogExtendedTableFilesLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ef_log_extended_table_files_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('log_id');
            $table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ef_log_extended_table_files_logs');
    }
}
```


## Defined routes
```php
// Api
ef-log/logs
ef-log/settings // GET|POST

```
