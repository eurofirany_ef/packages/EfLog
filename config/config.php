<?php

/*
 * You can place your custom package configuration in here.
 */
return [
    'app_name' => env('APP_NAME'),
    'app_env' => env('APP_ENV'),
    'app_url' => env('APP_URL'),
    'logs_url' => env('APP_URL').'/ef-log/logs/',
    'logs_extended_tables' => [
        //
    ],
    'logs_teams_channel_url' => env('EF_LOG_TEAMS_CHANNEL_URL'),
];
