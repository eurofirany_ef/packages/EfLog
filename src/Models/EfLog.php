<?php

namespace Eurofirany\EfLog\Models;

use Illuminate\Database\Eloquent\Model;

class EfLog extends Model
{
    protected $fillable = [
        'type',
        'title',
        'description',
        'group_id',
        'operation_id',
        'parent_id',
        'html'
    ];

    public function group()
    {
        return $this->belongsTo(EfLogGroup::class, 'group_id', 'id');
    }

    public function operation()
    {
        return $this->belongsTo(EfLogOperation::class, 'operation_id', 'id');
    }
}
