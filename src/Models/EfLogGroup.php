<?php

namespace Eurofirany\EfLog\Models;

use Illuminate\Database\Eloquent\Model;

class EfLogGroup extends Model
{
    protected $fillable = [
        'name',
        'translation'
    ];
}
