<?php

namespace Eurofirany\EfLog\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EfLogSetting extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'option',
        'value'
    ];
}
