<?php

namespace Eurofirany\EfLog\Models;

use Illuminate\Database\Eloquent\Model;

class EfLogOperation extends Model
{
    protected $fillable = [
        'name',
        'translation'
    ];
}
