<?php

namespace Eurofirany\EfLog\Console\Commands;

use Eurofirany\EfLog\Repositories\EfLogRepository;
use Eurofirany\EfLog\Repositories\EfLogOperationRepository;
use Illuminate\Console\Command;

class RemoveOperationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'EfLog:removeOperation {--name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete operation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(EfLogOperationRepository $efLogOperationRepository, EfLogRepository $efLogRepository)
    {
        $name = $this->option('name');

        if(!$name)
            $name = $this->ask('Enter a name of the operation you want to delete');

        $operationAssigned = $efLogRepository->getLogsByGroupId($name);

        if($operationAssigned) {
            throw new \RuntimeException('There are logs assigned to this operation!');
        }

        $efLogOperationRepository->deleteOperation($name)
            ? $this->info('Operation removed successfully.')
            : $this->error('Operation not found!');
    }
}
