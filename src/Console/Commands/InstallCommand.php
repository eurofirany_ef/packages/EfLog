<?php

namespace Eurofirany\EfLog\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'EfLog:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install package dependencies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        Artisan::call('vendor:publish --tag=ef_log');
        $this->info('Publishing complete.');

        Artisan::call('migrate');
        $this->info('Migration table created successfully.');

        Artisan::call('db:seed', [
            '--class' => "Eurofirany\EfLog\database\seeders\EfLogSettingSeeder"
        ]);
        $this->info('Database seeding completed successfully.');
    }
}
