<?php

namespace Eurofirany\EfLog\Console\Commands;

use Eurofirany\EfLog\Repositories\EfLogGroupRepository;
use Eurofirany\EfLog\Repositories\EfLogRepository;
use Illuminate\Console\Command;

class RemoveGroupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'EfLog:removeGroup {--name=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete group';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(EfLogGroupRepository $efLogGroupRepository, EfLogRepository $efLogRepository)
    {
        $name = $this->option('name');

        if(!$name)
            $name = $this->ask('Enter a name of the group you want to delete');

        $groupAssigned = $efLogRepository->getLogsByGroupId($name);

        if($groupAssigned) {
            throw new \RuntimeException('There are logs assigned to this group!');
        }

        $efLogGroupRepository->deleteGroup($name)
            ? $this->info('Group removed successfully.')
            : $this->error('Group not found!');
    }
}
