<?php

namespace Eurofirany\EfLog\Console\Commands;

use Eurofirany\EfLog\Repositories\EfLogOperationRepository;
use Illuminate\Console\Command;

class AddOperationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'EfLog:addOperation {--name=} {--translation=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new log operation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(EfLogOperationRepository $efLogOperationRepository)
    {
        $name = $this->option('name');

        if(!$name)
            $name = $this->ask('Enter a name of the operation');

        $translation = $this->option('translation');

        if(!$translation)
            $translation = $this->ask('Enter a translation for the operation name');

        $createOperation = $efLogOperationRepository->createOperation([
            'name' => $name,
            'translation' => $translation
        ]);

        $createOperation
            ? $this->info('Operation created successfully.')
            : $this->error('Operation already exists!');
    }
}
