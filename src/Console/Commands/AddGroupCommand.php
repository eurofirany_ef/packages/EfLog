<?php

namespace Eurofirany\EfLog\Console\Commands;

use Eurofirany\EfLog\Repositories\EfLogGroupRepository;
use Illuminate\Console\Command;

class AddGroupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'EfLog:addGroup {--name=} {--translation=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new log group';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(EfLogGroupRepository $efLogGroupRepository)
    {
        $name = $this->option('name');

        if(!$name)
            $name = $this->ask('Enter a name of the group');

        $translation = $this->option('translation');

        if(!$translation)
            $translation = $this->ask('Enter a translation for the group name');

        $createGroup = $efLogGroupRepository->createGroup([
            'name' => $name,
            'translation' => $translation
        ]);

        $createGroup
            ? $this->info('Group created successfully.')
            : $this->error('Group already exists!');
    }
}
