<?php

namespace Eurofirany\EfLog\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ExtendTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'EfLog:extend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create extend table for EfLog';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $table = $this->ask('Enter a table name');

        $tableName = 'EfLogExtendedTable' . $table . 'Log';

        Artisan::call('make:model ' . $tableName . ' -m');

        $this->info('Table created successfully.');
    }
}
