<?php

namespace Eurofirany\EfLog\Http\Controllers;

use App\Http\Controllers\Controller;
use Eurofirany\EfLog\Repositories\EfLogGroupRepository;
use Illuminate\Http\JsonResponse;

class EfLogGroupController extends Controller
{
    private EfLogGroupRepository $efLogGroupRepository;

    public function __construct(EfLogGroupRepository $efLogGroupRepository)
    {
        $this->efLogGroupRepository = $efLogGroupRepository;
    }

    public function index(): JsonResponse
    {
        return response()->json([
            $this->efLogGroupRepository->getGroups()
        ]);
    }
}

