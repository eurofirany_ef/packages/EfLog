<?php

namespace Eurofirany\EfLog\Http\Controllers;

use App\Http\Controllers\Controller;
use Eurofirany\EfLog\Repositories\EfLogRepository;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class EfLogController extends Controller
{
    private EfLogRepository $efLogRepository;

    public function __construct(EfLogRepository $efLogRepository)
    {
        $this->efLogRepository = $efLogRepository;
    }

    public function index(Request $request): JsonResponse
    {
        return response()->json([
            $this->efLogRepository->getLogs($request->type, $request->dateFrom, $request->dateTo, $request->groupId, $request->operationId)
        ]);
    }
}

