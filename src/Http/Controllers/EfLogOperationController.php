<?php

namespace Eurofirany\EfLog\Http\Controllers;

use App\Http\Controllers\Controller;
use Eurofirany\EfLog\Repositories\EfLogOperationRepository;
use Illuminate\Http\JsonResponse;

class EfLogOperationController extends Controller
{
    private EfLogOperationRepository $efLogOperationRepository;

    public function __construct(EfLogOperationRepository $efLogOperationRepository)
    {
        $this->efLogOperationRepository = $efLogOperationRepository;
    }

    public function index(): JsonResponse
    {
        return response()->json([
            $this->efLogOperationRepository->getOperations()
        ]);
    }
}

