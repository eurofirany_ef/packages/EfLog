<?php

namespace Eurofirany\EfLog\Http\Controllers;

use App\Http\Controllers\Controller;
use Eurofirany\EfLog\Repositories\EfLogSettingRepository;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class EfLogSettingController extends Controller
{
    private EfLogSettingRepository $efLogSettingRepository;

    public function __construct(EfLogSettingRepository $efLogSettingRepository)
    {
        $this->efLogSettingRepository = $efLogSettingRepository;
    }

    public function index(): JsonResponse
    {
        return response()->json([
            $this->efLogSettingRepository->getSettings()
        ]);
    }

    public function update(Request $request): JsonResponse
    {
        if (!$request->settings)
            abort('400', 'Settings array not found.');

        foreach ($request->settings as $option => $value)
            $this->efLogSettingRepository->updateSetting($option, $value);

        return response()->json([
            'Settings updated successfully.'
        ], 201);
    }
}

