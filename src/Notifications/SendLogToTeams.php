<?php

namespace Eurofirany\EfLog\Notifications;

use Illuminate\Notifications\Notification;
use NotificationChannels\MicrosoftTeams\MicrosoftTeamsChannel;
use NotificationChannels\MicrosoftTeams\MicrosoftTeamsMessage;

class SendLogToTeams extends Notification
{
    private string $type;
    private string $title;
    private ?string $content;
    private ?string $logUrl;

    public function __construct($data)
    {
        $this->type = $data['type'];
        $this->title = $data['title'];
        $this->content = $data['content'] ?? null;
        $this->logUrl = $data['logUrl'] ?? null;
    }

    public function via($notifiable): array
    {
        return [MicrosoftTeamsChannel::class];
    }

    public function toMicrosoftTeams($notifiable): MicrosoftTeamsMessage
    {
        $message = MicrosoftTeamsMessage::create()
            ->to(config('ef_log.logs_teams_channel_url'))
            ->type($this->type)
            ->title($this->title)
            ->content($this->content);

        if ($this->logUrl != null)
            $message->button('Pogląd', $this->logUrl);

        return $message;
    }

    public function routeNotificationForMicrosoftTeams(Notification $notification)
    {
        return config('ef_log.logs_teams_channel_url');
    }
}
