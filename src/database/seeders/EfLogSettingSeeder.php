<?php

namespace Eurofirany\EfLog\Database\Seeders;

use Eurofirany\EfLog\Models\EfLogSetting;
use Illuminate\Database\Seeder;

class EfLogSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EfLogSetting::updateOrCreate([
            'option' => 'save_logs',
            'value' => 1,
        ]);

        EfLogSetting::updateOrCreate([
            'option' => 'send_notifications',
            'value' => 1
        ]);
    }
}
