<?php

namespace Eurofirany\EfLog\Repositories;

use Eurofirany\EfLog\Models\EfLogGroup;

class EfLogGroupRepository
{
    public function model(): string
    {
        return EfLogGroup::class;
    }

    public function getGroups()
    {
        return EfLogGroup::get();
    }

    public function createGroup(array $group)
    {
        return EfLogGroup::firstOrCreate($group);
    }

    public function deleteGroup(string $name): bool
    {
        return EfLogGroup::where('name', $name)
            ->delete();
    }
}
