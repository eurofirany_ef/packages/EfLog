<?php

namespace Eurofirany\EfLog\Repositories;

use Eurofirany\EfLog\Models\EfLogOperation;

class EfLogOperationRepository
{
    public function model(): string
    {
        return EfLogOperation::class;
    }

    public function getOperations()
    {
        return EfLogOperation::all();
    }

    public function createOperation(array $operation)
    {
        return EfLogOperation::firstOrCreate($operation);
    }

    public function deleteOperation(string $name)
    {
        return EfLogOperation::where('name', $name)
            ->delete();
    }
}
