<?php

namespace Eurofirany\EfLog\Repositories;

use Eurofirany\EfLog\Models\EfLogSetting;

class EfLogSettingRepository
{
    public function model(): string
    {
        return EfLogSetting::class;
    }

    public function getSettings()
    {
        return EfLogSetting::all();
    }

    public function updateSetting(string $option, string $value): bool
    {
        return EfLogSetting::where('option', $option)
            ->update(['value' => $value]);
    }
}
