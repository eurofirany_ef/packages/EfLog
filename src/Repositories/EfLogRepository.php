<?php

namespace Eurofirany\EfLog\Repositories;

use Eurofirany\EfLog\Models\EfLog;

class EfLogRepository
{
    public function model(): string
    {
        return EfLog::class;
    }

    public function getLogs(?int $type, ?string $dateFrom, ?string $dateTo, ?int $groupId, ?int $operationId, ?string $filtersJson = '')
    {
        if (!$dateFrom)
            $dateFrom = date('Y-m-d', strtotime('-7 day'));

        if (!$dateTo)
            $dateTo = date('Y-m-d');

        // TODO extended tables queries
        $filters = json_decode($filtersJson, true);

        $efLogs = EfLog::with('group', 'operation')
            ->whereDate('ef_logs.created_at', '>=', $dateFrom)
            ->whereDate('ef_logs.created_at', '<=', $dateTo);

        $efLogs->when($type, function ($query, $type) {
            return $query->where('ef_logs.type', $type);
        });

        $efLogs->when($groupId, function ($query, $groupId) {
            return $query->where('ef_logs.group_id', $groupId);
        });

        $efLogs->when($operationId, function ($query, $operationId) {
            return $query->where('ef_logs.operation_id', $operationId);
        });

        $extendedTables = config('ef_log.logs_extended_tables');

        if (!empty($extendedTables)) {
            foreach ($extendedTables as $table) {
                $tablePath = $this->generateExtendedTablePath($table);
                $efLogs->leftJoin($tablePath, 'ef_logs.id', $tablePath . '.log_id');
            }
        }

        return $efLogs->get();
    }

    public function getLogsByGroupId(int $groupId)
    {
        return EfLog::with('group', 'operation')
            ->where('group_id', $groupId)
            ->get();
    }

    public function getLogById(int $logId)
    {
        $efLogs = EfLog::with('group', 'operation');

        $extendedTables = config('ef_log.logs_extended_tables');

        if (!empty($extendedTables)) {
            foreach ($extendedTables as $table) {
                $tablePath = $this->generateExtendedTablePath($table);
                $efLogs->leftJoin($tablePath, 'ef_logs.id', $tablePath . '.log_id');
            }
        }

        return $efLogs->find($logId);
    }

    public function createLog(array $log)
    {
        return EfLog::create($log);
    }

    private function generateExtendedTablePath($table): string
    {
        return 'ef_log_extended_table_' . strtolower($table) . '_logs';
    }
}
