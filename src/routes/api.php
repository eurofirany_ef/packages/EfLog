<?php

use Eurofirany\EfLog\Http\Controllers\EfLogGroupController;
use Illuminate\Support\Facades\Route;
use Eurofirany\EfLog\Http\Controllers\EfLogController;
use Eurofirany\EfLog\Http\Controllers\EfLogSettingController;
use Eurofirany\EfLog\Http\Controllers\EfLogOperationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'api/ef-log', 'middleware' => 'efApi'], function () {
    Route::get('/logs', [EfLogController::class, 'index']);

    Route::get('/settings', [EfLogSettingController::class, 'index']);
    Route::post('/settings', [EfLogSettingController::class, 'update']);

    Route::get('/groups', [EfLogGroupController::class, 'index']);

    Route::get('/operations', [EfLogOperationController::class, 'index']);
});
