<?php

namespace Eurofirany\EfLog;

use Illuminate\Support\Facades\Facade;

class EfLogFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'ef-log';
    }
}
