<?php

namespace Eurofirany\EfLog;

use Eurofirany\EfLog\Classes\EfLogType;
use Eurofirany\EfLog\Repositories\EfLogGroupRepository;
use Eurofirany\EfLog\Repositories\EfLogOperationRepository;
use Eurofirany\EfLog\Repositories\EfLogRepository;
use Eurofirany\EfLog\Notifications\SendLogToTeams;
use Eurofirany\EfLog\Repositories\EfLogSettingRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;

class EfLog extends EfLogType
{
    public const LOG_TYPE = [
        'info',
        'success',
        'danger',
        'warning'
    ];

    public ?array $properties = [];
    public ?string $description = '';
    public ?int $groupId = null;
    public ?string $groupName = null;
    public ?string $groupTranslation = null;
    public ?int $logId = null;
    public bool $notificationSent = false;
    public ?int $parentId = null;
    private Collection $settings;
    private ?int $operationId = null;
    private ?string $operationName;
    private ?string $operationTranslation;

    public function __construct()
    {
        $this->settings = collect();
    }

    public static function create(): EfLog
    {
        return new EfLog();
    }

    public function show(): void
    {
        echo view('vendor.ef-log.log', [
            'title' => $this->title,
            'description' => $this->description,
            'type' => self::LOG_TYPE[$this->getIntLogType()]
        ]);
    }

    public function description(string $description = ''): EfLog
    {
        $this->description = $description;

        return $this;
    }

    public function properties(string $table, array $properties = []): EfLog
    {
        $this->properties[$table] = collect([]);

        foreach ($properties as $propertyKey => $propertyValue)
            $this->properties[$table]->put($propertyKey, $propertyValue);

        return $this;
    }

    public function save(): EfLog
    {
        if ($this->getSetting('save_logs')->value != true) {
            return $this;
        }

        $efLogRepository = new EfLogRepository;

        $log = $efLogRepository->createLog([
            'type' => $this->getIntLogType(),
            'title' => $this->title,
            'description' => $this->description,
            'group_id' => $this->groupId,
            'operation_id' => $this->operationId,
            'parent_id' => $this->parentId,
            'html' => $this->prepareContent()
        ]);

        $this->setLogId($log->id);

        $this->saveProperties();

        return $this;
    }

    public function send(): EfLog
    {
        if ($this->getSetting('send_notifications')->value != true) {
            return $this;
        }

        $notification = [
            'type' => $this->logType,
            'title' => $this->title,
            'content' => $this->prepareContent(),
            'appUrl' => config('ef_log.app_url'),
            'appEnv' => config('ef_log.app_env')
        ];

        if ($this->logId != null)
            $notification['logUrl'] = $this->getLogUrl($this->logId);

        Notification::route('teams', 'MicrosoftTeams')->notify(new SendLogToTeams($notification));

        $this->notificationSent = true;

        return $this;
    }

    public function group(string $name): EfLog
    {
        $efLogGroupRepository = new EfLogGroupRepository;

        $groups = $efLogGroupRepository->getGroups();

        $group = $groups->firstWhere('name', $name);

        if (!$group)
            abort('422', 'Group "' . $name . '" does not exist in ef_log_groups table!');

        $this->groupId = $group->id;
        $this->groupName = $name;
        $this->groupTranslation = $group->translation;

        return $this;
    }

    public function operation(string $name): EfLog
    {
        $efLogOperationRepository = new EfLogOperationRepository;

        $operations = $efLogOperationRepository->getOperations();

        $operation = $operations->firstWhere('name', $name);

        if (!$operation)
            abort('422', 'Operation "' . $name . '" does not exist in ef_log_operations table!');

        $this->operationId = $operation->id;
        $this->operationName = $name;
        $this->operationTranslation = $operation->translation;

        return $this;
    }

    public function getExtendedTableModel(string $table)
    {
        $model = 'App\\Models\\EfLogExtendedTable' . $table . 'Log';

        $definedExtendedTables = config('ef_log.logs_extended_tables');

        if (!in_array($table, $definedExtendedTables)) {
            return abort('422', 'Extended table "' . $table . '" not set in ef_log configuration!');
        }

        try {
            return new $model;
        } catch (\Error $exception) {
            return abort('422', 'Model ' . $model . ' does not exist!');
        }
    }

    public function parent(int $parentId): EfLog
    {
        $this->parentId = $parentId;

        return $this;
    }

    private function saveProperties(): void
    {
        foreach ($this->properties as $table => $properties) {
            $model = $this->getExtendedTableModel($table);

            $propertiesArray = $properties->toArray();
            $propertiesArray['log_id'] = $this->logId;

            $model::create($propertiesArray);
        }
    }

    private function setLogId(int $logId): void
    {
        $this->logId = $logId;
    }

    private function prepareProperties(): string
    {
        $textProperties = '';

        foreach ($this->properties as $properties)
            foreach ($properties as $propertyKey => $propertyValue)
                $textProperties .= '<pre>' . $propertyKey . ': ' . $propertyValue . '</pre>';

        return $textProperties;
    }

    private function prepareContent(): string
    {
        $content = $this->title . '<br>';
        $content .= $this->description . '<br><br>';

        if ($this->groupTranslation != null)
            $content .= '<b>Grupa:</b> ' . $this->groupTranslation . '<br>';

        if ($this->operationTranslation != null)
            $content .= '<b>Operacja:</b> ' . $this->operationTranslation . '<br>';

        if (count($this->properties))
            $content .= '<b>Właściwości: </b><br>' . $this->prepareProperties();

        $content .= '<hr>';
        $content .= 'Nazwa aplikacji: ' . config('ef_log.app_name') . '<br>';
        $content .= 'Adres URL: ' . config('ef_log.app_url') . '<br>';
        $content .= 'Środowisko: ' . config('ef_log.app_env');

        return $content;
    }

    private function getLogUrl(int $logId): string
    {
        return config('ef_log.logs_url') . '?token=' . config('ef_log.logs_token') . '&logId=' . $logId;
    }

    private function setSettings(): void
    {
        $efLogSettingRepository = new EfLogSettingRepository();

        $this->settings = $efLogSettingRepository->getSettings();
    }

    private function getSetting(string $name)
    {
        if ($this->settings->isEmpty())
            $this->setSettings();

        return $this->settings->firstWhere('option', $name);
    }

    public function __destruct()
    {
        if ($this->getSetting('save_logs')->value == 2 && $this->logId == null)
            $this->save();

        if ($this->getSetting('send_notifications')->value == 2 && $this->notificationSent == false)
            $this->send();
    }
}

