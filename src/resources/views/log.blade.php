<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app_name') }}</title>

    <!-- Styles -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="{{ asset('vendor/ef-log/css/style.css') }}" rel="stylesheet">
</head>
<body class="text-center">
<div class="content shadow-lg p-5 rounded">
    <main role="main" class="inner cover"
          style="height:100%; display:flex; justify-content: center; align-items: center;">
        <h3 class="cover-heading">{{ __($title) }}</h3>
    </main>
    <div class="mx-auto d-block">
        <p class="lead">{{ __($description) }}</p>
        <p class="lead mt-2">
            <a href="{{ url()->previous() }}" class="btn btn-{{ $type }}">{{ __('back') }}</a>
        </p>
    </div>
</div>
</body>
</html>
