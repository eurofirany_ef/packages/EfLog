<?php

namespace Eurofirany\EfLog\Classes;

abstract class EfLogType
{
    private const LOG_TYPES = [
        'info',
        'success',
        'error',
        'warning',
    ];

    public string $logType;
    public string $title;

    public function info(string $title): object
    {
        $this->setLogType(0);
        $this->setTitle($title);

        return $this;
    }

    public function success(string $title): object
    {
        $this->setLogType(1);
        $this->setTitle($title);

        return $this;
    }

    public function error(string $title): object
    {
        $this->setLogType(2);
        $this->setTitle($title);

        return $this;
    }

    public function warning(string $title): object
    {
        $this->setLogType(3);
        $this->setTitle($title);

        return $this;
    }

    public function getIntLogType(): int
    {
        return array_search($this->logType, self::LOG_TYPES);
    }

    private function setLogType(int $intLogType): void
    {
        $this->logType = self::LOG_TYPES[$intLogType];
    }

    private function setTitle(string $title): void
    {
        $this->title = $title;
    }
}
