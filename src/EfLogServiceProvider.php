<?php

namespace Eurofirany\EfLog;

use Eurofirany\EfLog\Console\Commands\AddGroupCommand;
use Eurofirany\EfLog\Console\Commands\InstallCommand;
use Eurofirany\EfLog\Console\Commands\ExtendTableCommand;
use Eurofirany\EfLog\Console\Commands\AddOperationCommand;
use Eurofirany\EfLog\Console\Commands\RemoveGroupCommand;
use Eurofirany\EfLog\Console\Commands\RemoveOperationCommand;
use Illuminate\Support\ServiceProvider;

class EfLogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $this->loadJSONTranslationsFrom(__DIR__ . '/resources/lang');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'ef-log');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');

        if ($this->app->runningInConsole()) {
            // Publishing the views.
            $this->publishes([
                __DIR__ . '/resources/views' => resource_path('views/vendor/ef-log'),
            ], 'ef_log');

            // Publishing assets.
            $this->publishes([
                __DIR__ . '/resources/assets' => public_path('vendor/ef-log'),
            ], 'ef_log');

            // Publishing the translation files.
            $this->publishes([
                __DIR__ . '/resources/lang' => resource_path('lang/vendor/ef-log'),
            ], 'ef_log');

            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('ef_log.php'),
            ], 'ef_log');

            // Registering package commands.
            $this->commands([
                InstallCommand::class,
                ExtendTableCommand::class,
                AddOperationCommand::class,
                RemoveOperationCommand::class,
                AddGroupCommand::class,
                RemoveGroupCommand::class
            ]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'ef-log');

        // Register the main class to use with the facade
        $this->app->singleton('ef-log', function () {
            return new EfLog;
        });
    }
}
